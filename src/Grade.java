public class Grade {

   public static void main (String[] p) {
      double percent;
      while (true) {
         System.out.print ("Input percentage: ");
         percent = TextIO.getlnDouble();
         System.out.print (percent + ": ");
         System.out.println (grade (percent));
      } // while
   } // main


   public static String grade (double d) {
      if (d < 0)
    	  throw new IllegalArgumentException ("negatiivne protsent " + d);
      
      if (d > 150)
    	  throw new IllegalArgumentException ("liiga suur protsent: " + d);
      
      if (d <= 50)
    	  return "fail";
      
      if (50.5 <= d && 60 >= d)
    	  return "Hinne 1";
      
      if (d >= 60.5 && 70 >= d)
    	  return "Hinne 2";
      
      if (d >= 70.5 && 80 >= d)
    	  return "Hinne 3";
      
      if (d >= 80.5 && 90 >= d)
    	  return "Hinne 4";
      
      if (d >= 90.5 && 150 >= d)
    	  return "Hinne 5";
      
      return "";
   }
}

